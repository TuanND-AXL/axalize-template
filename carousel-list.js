import { useRouter } from 'next/router';
import { useRef, useState } from 'react';
import Link from '../components/Link';

const CarouselList = ({ categories }) => {
  const router = useRouter();
  const [scrollPosition, setScrollPosition] = useState(0);
  const [isDragging, setIsDragging] = useState(false);
  const [isMoving, setIsMoving] = useState(false);
  const [startX, setStartX] = useState(0);
  const listRef = useRef(null);

  const handleMouseDown = (event) => {
    setIsDragging(true);
    setStartX(event.pageX);
    listRef.current.style.cursor = 'grabbing';
  };

  const handleMouseMove = (event) => {
    if (isDragging) {
      const deltaX = event.pageX - startX;
      listRef.current.scrollLeft = scrollPosition - deltaX;
      setIsMoving(true);
    }
  };

  const handleMouseUp = () => {
    setTimeout(() => {
      setIsMoving(false);
    }, 10);
    setIsDragging(false);
    setScrollPosition(listRef.current.scrollLeft);
    listRef.current.style.cursor = 'grab';
  };

  return (
    <>
      <div className="categories">
        <div
          className="category-list"
          ref={listRef}
          onMouseDown={handleMouseDown}
          onMouseMove={handleMouseMove}
          onMouseUp={handleMouseUp}
          onMouseLeave={handleMouseUp}
        >
          <Link href={isMoving ? router.asPath : '/'}>
            <a className={`category-item ${router.asPath === '/' ? 'active' : ''}`}>Trang chủ</a>
          </Link>
          {categories.map(c => (
            <Link href={isMoving ? router.asPath : ('/' + c.data.id + '-' + c.data.slug)} key={c.data.id}>
              <a className={`category-item ${router.asPath === ('/' + c.data.id + '-' + c.data.slug) ? 'active' : ''}`} key={c.data.id}>{c.data.name}</a>
            </Link>
          ))}
        </div>
      </div>
      <style jsx>
        {`
          .categories {
            width: 100%;
            background-color: #FD9426;
            height: 2.75rem;
            user-select: none;
          }
          .category-list {
            max-width: calc(1170px - 4rem);
            width: 100%;
            overflow: auto;
            white-space: nowrap;
            margin: auto;
            display: flex;
            gap: .75rem;
          }
          .category-list::-webkit-scrollbar-track {
            display: none;
          }

          .category-list::-webkit-scrollbar {
            display: none;
          }

          .category-list::-webkit-scrollbar-thumb {
            display: none;
          }
          .category-item {
            position: relative;
            width: 107px;
            min-width: 107px;
            max-width: 107px;
            text-align: center;
            font-weight: 600;
            line-height: 2.75rem;
            color: #fff;
            text-decoration: none;
            -webkit-user-drag: none;
          }
          .category-item.active {
            font-weight: 800;
          }
          .category-item.active:before {
            content: "";
            height: 2px;
            width: 100%;
            background: #fff;
            position: absolute;
            bottom: 0;
            left: 0;
          }
        `}
      </style>
    </>
  )
}

export default CarouselList;
